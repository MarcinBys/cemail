﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CEmail
{
    /// <summary>
    /// Logika interakcji dla klasy LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Zaloguj.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.LoggedIn = ImapService.Login(username.Text, password.Password);

            if (MainWindow.LoggedIn)    // jesli udalo sie zalogowac
            {
                MainWindow.MainFrame.Content = new HomePage();          // wyswietl strone glowna
            }
            else                        // jesli wystapil problem
            {
                error.Text = "Nie udało zalogować się do konta Gmail."; // wyswietl komunikat o bledzie
            }
        }
    }
}
