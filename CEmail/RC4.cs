﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEmail
{
    public static class RC4
    {
        public static string Encrypt(string message, string key)
        {
            StringBuilder cipher = new StringBuilder();
            int[] S = new int[256];     // substitution box

            int j = 0;
            int temp;
            int y;
            int t;

            for (int i = 0; i < 256; i++)
            {
                S[i] = i;
            }

            for (int i = 0; i < 256; i++)
            {
                j = (j + S[i] + key[i % key.Length]) % 256;

                // swap values
                temp = S[i];
                S[i] = S[j];
                S[j] = temp;
            }

            for (int i = 0; i < message.Length; i++)
            {
                y = (i + 1) % 256;
                j = (j + S[y]) % 256;

                // swap values
                temp = S[y];
                S[y] = S[j];
                S[j] = temp;

                t = S[(S[y] + S[j]) % 256];

                cipher.Append((char)(message[i] ^ t));
            }
            return cipher.ToString();
        }
    }
}
