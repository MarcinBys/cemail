﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImapX;
using ImapX.Collections;
using System.Windows;

namespace CEmail
{
    class ImapService
    {
        private static string[] credentials;

        public static ImapClient client
        {
            get;
            set;
        }

        public static void Initialize()
        {
            client = new ImapClient("imap.gmail.com", true);

            if (!client.Connect())
            {
                throw new Exception("Error connecting to the client.");
            }
        }

        public static bool Login(string username, string password)
        {
            try
            {
                credentials = new string[2] { username, password };

                return client.Login(username, password);
            }
            catch
            {
                return false;
            }
        }

        public static MessageCollection GetMessagesForFolder(string name="INBOX")
        {
            client.Folders[name].Messages.Download();
            return client.Folders[name].Messages;
        }

        public static string GetUsername()
        {
            if (credentials[0].Contains("@"))       // jesli uzytkownik wpisal calego username (czyli wraz z @gmail.com)
            {
                string[] username;

                username = credentials[0].Split('@');

                return username[0];
            }
            else
            {
                return credentials[0];
            }

        }

        public static string GetPassword()
        {
            return credentials[1];
        }
    }
}