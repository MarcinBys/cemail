﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Microsoft.VisualStudio.Utilities;


namespace SEAL
{
    class Program
    {
        static Encoding encoding = Encoding.GetEncoding("UTF-8");


        static string key = "0123456789012345678901234567890123456789";        // secret key

        static string H0, H1, H2, H3, H4;
        static UInt32 A, B, C, D, E;
        static UInt32 n1, n2, n3, n4;

        static uint L;
        static int[] T;
        static int[] S;
        static int[] R;



        static void Main(string[] args)
        {
            StringBuilder textToCipher = new StringBuilder();
            textToCipher.Append("Lorem Ipsum jest tekstem stosowanym jako Lorem Ipsum jest tekstem stosowanym jako");
            
            char[] at = textToCipher.ToString().ToCharArray();
            //Console.WriteLine("at len: " + at.Length);
            int i = at.Length;
            L = (uint)(i * 8);
            int[] text = new int[(int)Math.Ceiling((float)i / 4)];  //kodujemy, rozmiar jest 4 razy mniejszy

            for (int j = 0; j < Math.Ceiling((float)i / 4); j++)
            {
                int tmp = 0;
                for (int k = 0; k < 4; k++)
                {
                    int num = 4 * j + k;
                    tmp <<= 8;
                    if (num < i)    // jeśli tekst jeszcze się nie skończył
                        tmp += at[num];
                }
                text[j] = tmp;
            }


            // === ENCODING ===

            int[] enc = Coding(text, L, key, 1);


            /*
            StringBuilder encodedCipher = new StringBuilder();
            encodedCipher.Append(encoding.GetString(Encoding.Convert(Encoding.Default, encoding, GetByteArrayFromIntArray(enc))));
            Console.WriteLine(encodedCipher);
            */

            StringBuilder encodedCipher = new StringBuilder();

            byte[] coded = new byte[4 * enc.Length];         // powinno byc [L / 8];
            int c = 0;  // kodowany indeks
            for (int j = 0; j < Math.Ceiling((float)L / 32); j++)
            {
                for (int s = 3; s >= 0; s--)
                {
                    coded[c++] = (byte)(enc[j] >> (8 * s));
                }
            }
            //Console.WriteLine("c: " + c);

            //Console.WriteLine("coded len: " + coded.Length);

            Console.WriteLine("Encoded:");
            encodedCipher.Append(encoding.GetString(Encoding.Convert(Encoding.Default, encoding, coded)));
            Console.WriteLine(encodedCipher);
            //Console.WriteLine("encodedCipher len: " + encodedCipher.ToString().Length);

            // byte array coded konwertowany jest na string
            // string jest wyswietlany




            /*
            StringBuilder textToDecipher = new StringBuilder();
            textToDecipher.Append(encodedCipher.ToString());

            //Console.WriteLine("encodedCipher len: " + encodedCipher.ToString());

            byte[] at2 = encoding.GetBytes(textToDecipher.ToString().ToCharArray());
            Console.WriteLine("at2 len: " + at2.Length);
            int i2 = at2.Length;
            L = (uint)(i2 * 8);
            int[] text2 = new int[(int)Math.Ceiling((float)i2 / 4)];  //kodujemy, rozmiar jest 4 razy mniejszy

            for (int j = 0; j < Math.Ceiling((float)i2 / 4); j++)
            {
                int temp = 0;
                for (int k = 0; k < 4; k++)
                {
                    int num = 4 * j + k;
                    temp <<= 8;
                    if (num < i2)//jeśli tekst jeszcze się nie skończył
                        temp += at2[num];
                }
                text2[j] = temp;
            }
            */


            /*byte[] encodedCipherByteArray = encoding.GetBytes(encodedCipher.ToString().ToCharArray());
            Console.WriteLine("encodedCipherByteArray len:" + encodedCipherByteArray.Length);

            int[] encodedCipherIntArray = new int[encodedCipherByteArray.Length / 32];
            for (int j = 0; j < encodedCipherByteArray.Length / 32; j++)
            {
                encodedCipherIntArray[j] = BitConverter.ToInt32(encodedCipherByteArray, j * 32);
            }
            Console.WriteLine("encodedCipherIntArray len:" + encodedCipherIntArray.Length);

            Console.WriteLine("enc[0]" + enc[0]);
            Console.WriteLine("encodedCipherIntArray[0]" + encodedCipherIntArray[0]);*/

            // === DECODING ===

            int[] dec = Coding(enc, L, key, 1);
            //Console.WriteLine("enc len:" + enc.Length);
            //Console.WriteLine("dec len:" + dec.Length);


            /*
            StringBuilder decodedCipher = new StringBuilder();
            decodedCipher.Append(encoding.GetString(Encoding.Convert(Encoding.Default, encoding, GetByteArrayFromIntArray(dec))));
            Console.WriteLine(decodedCipher);
            */


            StringBuilder decodedCipher = new StringBuilder();

            byte[] decoded = new byte[4 * dec.Length];         // powinno byc [L / 8];
            int d = 0;  // dekodowany indeks
            for (int j = 0; j < Math.Ceiling((float)L / 32); j++)
            {
                for (int s = 3; s >= 0; s--)
                {
                    decoded[d++] = (byte)(dec[j] >> (8 * s));
                }
            }

            Console.WriteLine("Decoded:");
            decodedCipher.Append(encoding.GetString(Encoding.Convert(Encoding.Default, encoding, decoded)));
            Console.WriteLine(decodedCipher);

            Console.ReadKey();
        }

        static void G(string a, UInt32 i)
        {
            UInt32[] y = new UInt32[4] { 0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6 };

            UInt32[] X = new UInt32[80];

            X[0] = i;

            for (int j = 1; j <= 15; j++)
            {
                X[j] = 0x00000000;
            }

            for (int j = 16; j <= 79; j++)
            {
                X[j] = (BitRotator.RotateLeft(X[j - 3] ^ X[j - 8] ^ X[j - 14] ^ X[j - 16], 1));
            }

            H0 = a.Substring(0, 8);
            H1 = a.Substring(8, 8);
            H2 = a.Substring(16, 8);
            H3 = a.Substring(24, 8);
            H4 = a.Substring(32, 8);

            A = Convert.ToUInt32(H0);
            B = Convert.ToUInt32(H1);
            C = Convert.ToUInt32(H2);
            D = Convert.ToUInt32(H3);
            E = Convert.ToUInt32(H4);

            // Round 1
            for (int j = 0; j <= 19; j++)
            {
                var temp = (BitRotator.RotateLeft(A, 5) + ((B & C) | (~B & D)) + E + X[j] + y[0]);

                A = Convert.ToUInt32(temp);
                B = A;
                C = BitRotator.RotateLeft(B, 30);
                D = C;
                E = D;
            }
            
            // Round 2
            for (int j = 20; j <= 39; j++)
            {
                var temp = (BitRotator.RotateLeft(A, 5) + (B ^ C ^ D) + E + X[j] + y[1]);

                A = Convert.ToUInt32(temp);
                B = A;
                C = BitRotator.RotateLeft(B, 30);
                D = C;
                E = D;
            }

            // Round 3
            for (int j = 40; j <= 59; j++)
            {
                var temp = (BitRotator.RotateLeft(A, 5) + ((B & C) | (B & D) | (C & D)) + E + X[j] + y[2]);

                A = Convert.ToUInt32(temp);
                B = A;
                C = BitRotator.RotateLeft(B, 30);
                D = C;
                E = D;
            }

            // Round 4
            for (int j = 60; j <= 79; j++)
            {
                var temp = (BitRotator.RotateLeft(A, 5) + (B ^ C ^ D) + E + X[j] + y[3]);

                A = Convert.ToUInt32(temp);
                B = A;
                C = BitRotator.RotateLeft(B, 30);
                D = C;
                E = D;
            }

            H0 += A;
            H1 += B;
            H2 += C;
            H3 += D;
            H4 += E;
        }
        static int Fa(int i)
        {
            G(key, Convert.ToUInt32(Math.Floor((double)i / 5)));

            string[] H = new string[5];

            for(int j = 0; j<H.Length; j++)
            {
                H[j] = key.Substring(8 * j, 8);
            }

            return Convert.ToInt32(H[i % 5]);
        }


        static void TableGeneration()
        {
            T = new int[512];
            S = new int[256];
            int Rlength = Convert.ToInt32(4 * Math.Ceiling(Convert.ToDouble(((float)L - 1) / 8192)));
            R = new int[Rlength];

            for(int i = 0; i<T.Length; i++)
            {
                T[i] = Fa(i);
            }

            for(int j = 0; j< S.Length; j++)
            {
                S[j] = Fa(0x00001000 + j);
            }

            for (int k = 0; k < R.Length; k++)
            {
                R[k] = Fa(0x00002000 + k);
            }
        }


        static void InitializationProcedure(int n, int l)
        {
            A = (uint)(n ^ R[4 * l]);
            B = (uint)(BitRotator.RotateRight(n, 8) ^ (R[(4 * l) + 1]));
            C = (uint)(BitRotator.RotateRight(n, 16) ^ (R[(4 * l) + 2]));
            D = (uint)(BitRotator.RotateRight(n, 24) ^ (R[(4 * l) + 3]));

            UInt32 P;

            for (int j = 1; j <= 2; j++)
            {
                P = A & 0x000007fc;
                B += Convert.ToUInt32(T[P / 4]);
                A = BitRotator.RotateRight(A, 9);

                P = B & 0x000007fc;
                C += Convert.ToUInt32(T[P / 4]);
                B = BitRotator.RotateRight(B, 9);

                P = C & 0x000007fc;
                D += Convert.ToUInt32(T[P / 4]);
                C = BitRotator.RotateRight(C, 9);

                P = D & 0x000007fc;
                A += Convert.ToUInt32(T[P / 4]);
                D = BitRotator.RotateRight(D, 9);
            }

            n1 = D;
            n2 = B;
            n3 = A;
            n4 = C;

            P = A & 0x000007fc;
            B += Convert.ToUInt32(T[P / 4]);
            A = BitRotator.RotateRight(A, 9);

            P = B & 0x000007fc;
            C += Convert.ToUInt32(T[P / 4]);
            B = BitRotator.RotateRight(B, 9);

            P = C & 0x000007fc;
            D += Convert.ToUInt32(T[P / 4]);
            C = BitRotator.RotateRight(C, 9);

            P = D & 0x000007fc;
            A += Convert.ToUInt32(T[P / 4]);
            D = BitRotator.RotateRight(D, 9);
        }

        static int[] Seal(string a, int n, UInt32 L)
        {
            TableGeneration();

            int len = (int)(Math.Ceiling( (float)L / 128) * 4);
            int[] y = new int[len];

            for (int i = 0; i < len; i++)
            {
                y[i] = 0;
            }

            int k = 0;

            for (int l = 0; ; l++)
            {
                InitializationProcedure(n, l);

                for (int i = 1; i <= 64; i++)
                {
                    UInt32 P = A & 0x000007fc;
                    B += Convert.ToUInt32(T[P / 4]);
                    A = BitRotator.RotateRight(A, 9);
                    B ^= A;

                    UInt32 Q = B & 0x000007fc;
                    C ^= Convert.ToUInt32(T[Q / 4]);
                    B = BitRotator.RotateRight(B, 9);
                    C += B;

                    P = (P + C) & 0x000007fc;
                    D += Convert.ToUInt32(T[P / 4]);
                    C = BitRotator.RotateRight(C, 9);
                    D ^= C;

                    Q = (Q + D) & 0x000007fc;
                    A ^= Convert.ToUInt32(T[Q / 4]);
                    D = BitRotator.RotateRight(D, 9);
                    A += D;

                    P = (P + A) & 0x000007fc;
                    B ^= Convert.ToUInt32(T[P / 4]);
                    A = BitRotator.RotateRight(A, 9);

                    Q = (Q + B) & 0x000007fc;
                    C += Convert.ToUInt32(T[Q / 4]);
                    B = BitRotator.RotateRight(B, 9);

                    P = (P + C) & 0x000007fc;
                    D ^= Convert.ToUInt32(T[P / 4]);
                    C = BitRotator.RotateRight(C, 9);

                    Q = (Q + D) & 0x000007fc;
                    A += Convert.ToUInt32(T[Q / 4]);
                    D = BitRotator.RotateRight(D, 9);


                    y[k++] = (int)(B + S[(4 * i) - 4]);
                    y[k++] = (int)(C ^ S[(4 * i) - 3]);
                    y[k++] = (int)(D + S[(4 * i) - 2]);
                    y[k++] = (int)(A ^ S[(4 * i) - 1]);

                    if (k >= Math.Ceiling((float)L / 32))
                    {
                        return y;
                    }

                    if (i % 2 == 0)
                    {
                        A += n1;
                        C += n2;
                    }
                    else
                    {
                        A += n3;
                        C += n4;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">text to (en/de)code</param>
        /// <param name="L"></param>
        /// <param name="a">secret key</param>
        /// <param name="n">number</param>
        /// <returns></returns>
        static int[] Coding(int[] text, uint L, string a, int n)
        {
            int[] coded = new int[(int)Math.Ceiling((float)L / 32)];
            int[] y;

            y = Seal(a, n, L);

            for (int i = 0; i < Math.Ceiling((float)L / 32); i++)
            {
                coded[i] = text[i] ^ y[i];
            }

            return coded;
        }


        static byte[] GetByteArrayFromIntArray(int[] intArray)
        {
            byte[] data = new byte[intArray.Length * 4];

            for (int i = 0; i < intArray.Length; i++)
                Array.Copy(BitConverter.GetBytes(intArray[i]), 0, data, i * 4, 4);

            return data;
        }
    }
}