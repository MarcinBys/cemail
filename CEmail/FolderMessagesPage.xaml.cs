﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ImapX;

namespace CEmail
{
    /// <summary>
    /// Logika interakcji dla klasy FolderMessagesPage.xaml
    /// </summary>

    public partial class FolderMessagesPage : Page
    {
        public FolderMessagesPage(string name)
        {
            InitializeComponent();

            GetMailsList();
        }

        /// <summary>
        /// Zaladuj zawartosc wiadomosci po wybraniu jej z listy.
        /// </summary>
        private void messagesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var message = (sender as ListView).SelectedItem as Message;

            if (message != null)
            {
                HomePage.ContentFrame.Content = new MessagePage(message);
            }
        }

        /// <summary>
        /// Zaladuj liste wszystkich maili znajdujacych sie w skrzynce odbiorczej.
        /// </summary>
        private void GetMailsList()
        {
            messagesList.ItemsSource = ImapService.GetMessagesForFolder();
        }
    }
}
