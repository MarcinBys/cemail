﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ImapX;

namespace CEmail
{
    /// <summary>
    /// Logika interakcji dla klasy MessagePage.xaml
    /// Klasa wyświetlająca wybraną przez użytkownika wiadomość email.
    /// </summary>

    public partial class MessagePage : Page
    {
        /// <summary>
        /// Wypelnij ramke zawartoscia wybranej wiadomosci.
        /// </summary>
        /// <param name="message">Wiadomosc do zaladowania</param>
        public MessagePage(Message message)
        {
            InitializeComponent();

            subject.Text = message.Subject;
            from.Text = message.From.ToString();
            time.Text = message.Date.Value.ToString();
            body.Text = message.Body.Text;

            if (message.Attachments.Count() > 0)
            {
                attachments.Text = $"{message.Attachments.Count()} załączników w wiadomości.";
            }
            else
            {
                attachments.Text = "Brak załączników.";
            }
        }

        /// <summary>
        /// Odszyfruj zaszyfrowana wiadomosc.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DecryptButton_Click(object sender, RoutedEventArgs e)
        {
            error.Text = "";

            if (rc4_Button.IsChecked == true)
            {
                if (decryptKey.Text.Length <= 0)
                {
                    error.Text = "Podano błędną ilość znaków szyfru!";
                }
                else
                {
                    body.Text = RC4.Encrypt(body.Text, decryptKey.Text);
                }
            }
            else if (wake_Button.IsChecked == true)
            {
                if (decryptKey.Text.Length != 16)
                {
                    error.Text = "Należy podać 16 literowy ciąg znaków!";
                }
                else
                {
                    body.Text = WAKE.Encrypt(body.Text, decryptKey.Text);
                }
            }
            else if (seal_Button.IsChecked == true)
            {
                //bodyText = SEAL.Encrypt();
            }
        }

        private void rc4_Button_OnCheck(object sender, RoutedEventArgs e)
        {
            decryptKey.Text = "klucz";
        }

        private void wake_Button_OnCheck(object sender, RoutedEventArgs e)
        {
            decryptKey.Text = "kalilubijescowoc";
        }
    }
}