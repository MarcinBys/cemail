﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CEmail
{
    /// <summary>
    /// Logika interakcji dla klasy CreateAndSend.xaml
    /// </summary>
    public partial class CreateAndSend : Page
    {
        public CreateAndSend()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Wyslij wiadomosc
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            if ((rc4_Button.IsChecked == true && key.Text.Length <= 0) || 
                (wake_Button.IsChecked == true && key.Text.Length != 16) ||
                (to.Text != "" && !to.Text.Contains("@")))
            {
                hint.Text = "Coś poszło nie tak. Sprawdź podane dane.";
            }
            else
            {
                using (SmtpClient client = new SmtpClient("smtp.gmail.com", 587))
                {
                    // Konfiguracja klienta
                    client.EnableSsl = true;
                    client.Credentials = new NetworkCredential(ImapService.GetUsername(), ImapService.GetPassword());

                    // Szyfrowanie wiadomosci
                    string bodyText = string.Empty;

                    if (noCipher_Button.IsChecked == true)
                    {
                        bodyText = body.Text;
                    }
                    else if (rc4_Button.IsChecked == true)
                    {
                        bodyText = RC4.Encrypt(body.Text, key.Text);
                    }
                    else if (wake_Button.IsChecked == true)
                    {
                        bodyText = WAKE.Encrypt(body.Text, key.Text);
                    }
                    else if (seal_Button.IsChecked == true)
                    {
                        //bodyText = SEAL.Encrypt();
                    }

                    // Jesli pole odbiorcy jest puste, wyslij do samego siebie
                    if (to.Text == "")
                    {
                        to.Text = ImapService.GetUsername() + "@gmail.com";
                    }

                    // Tworzenie obiektu MailMessage
                    MailMessage message = new MailMessage(
                                             ImapService.GetUsername() + "@gmail.com",  // od
                                             to.Text,                                   // do
                                             subject.Text,                              // tytul
                                             bodyText                                   // tresc
                                          );

                    // Wysylanie wiadomosci
                    client.Send(message);

                    // Oczyszczanie ramki po wyslaniu wiadomosci email.
                    HomePage.Clear();
                }
            }
        }

        /// <summary>
        /// Anuluj tworzenie nowej wiadomosci.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            HomePage.Clear();
        }

        private void noCipher_Button_OnCheck(object sender, RoutedEventArgs e)
        {
            hint.Text = "";

            key.IsEnabled = false;
            key.Text = "";
        }

        private void rc4_Button_OnCheck(object sender, RoutedEventArgs e)
        {
            hint.Text = "Wskazówka: podaj sekretny ciąg znaków";

            key.IsEnabled = true;
            key.Text = "klucz";
        }

        private void wake_Button_OnCheck(object sender, RoutedEventArgs e)
        {
            hint.Text = "Wskazówka: podaj sekretny, 16 literowy ciąg znaków";

            key.IsEnabled = true;
            key.Text = "kalilubijescowoc";
        }
    }
}