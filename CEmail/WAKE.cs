﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEmail
{
    class WAKE
    {
        static uint[] T = new uint[257];      // krok 1, S-box

        static string[] TT = new string[8] { "726a8f3b", "e69a3b5c", "d3c71fe5", "ab3c73d2", "4d3a8eb3", "0396d6e8", "3d4c2f7a", "9ee27cf3" };

        static Encoding encoding = Encoding.GetEncoding("UTF-8");

        public static string Encrypt(string text, string key)
        {
            StringBuilder cipher = new StringBuilder();

            string[] K = new string[4];
            string hexKey = StringToHex(key);
            uint X;

            // krok 2
            for (int i = 0; i < 4; i++)
            {
                K[i] = hexKey.Substring(i * 8, 8);      // podzial klucza na 4 czesci

                T[i] = StringToUint(K[i]);              // inicjalizacja 4 poczatkowych wartosci S-boxa
            }

            // krok 3
            for (int i = 4; i < 256; i++)
            {
                X = T[i - 4] + T[i - 1];
                T[i] = X >> 3 ^ StringToUint(TT[X & 7]);
            }

            // krok 4
            for (int i = 0; i < 23; i++)
            {
                T[i] = T[i] + T[i + 89];
            }

            // krok 5
            X = T[33];
            uint Z = T[59] | 0x01000001;
            Z = Z & 0xFF7FFFFF;
            X = (X & 0xFF7FFFFF) + Z;

            // krok 6
            for (int i = 0; i < 256; i++)
            {
                X = (X & 0xFF7FFFFF) + Z;
                T[i] = T[i] & 0x00FFFFFF ^ X;
            }

            // krok 7
            T[256] = T[0];
            X = X & 255;

            // krok 8
            for (int i = 0; i < 256; i++)
            {
                uint temp = (T[i ^ X] ^ X) & 255;
                T[i] = T[temp];
                T[X] = T[i + 1];
            }

            List<string> a = new List<string>();
            a.Add(K[0]);

            List<string> b = new List<string>();
            b.Add(K[1]);

            List<string> c = new List<string>();
            c.Add(K[2]);

            List<string> d = new List<string>();
            d.Add(K[3]);


            for (int i = 0; i < 1; i++)
            {
                a.Add(M(a[i], d[i]));
                b.Add(M(b[i], a[i + 1]));
                c.Add(M(c[i], b[i + 1]));
                d.Add(M(d[i], c[i + 1]));
            }

            byte[] asciiText = Encoding.Default.GetBytes(text);
            byte[] asciiKey = StringToByteArray(d[1]);

            uint[] cipherXOR = new uint[asciiText.Length];
            byte[] cipherAsciiBytes = new byte[asciiText.Length];

            for (int i = 0; i < cipherXOR.Length; i++)
            {
                cipherXOR[i] = (uint)asciiText[i] ^ (uint)asciiKey[i % 4];

                cipherAsciiBytes[i] = (byte)cipherXOR[i];
            }

            cipher.Append(encoding.GetString(Encoding.Convert(Encoding.Default, encoding, cipherAsciiBytes)));
            
            return cipher.ToString();
        }

        private static string M(string x, string y)
        {
            uint sumXY = StringToUint(x) + StringToUint(y);

            uint leftHandSide = sumXY >> 8;

            uint rightHandSide = T[sumXY & 255];

            string xor = (leftHandSide ^ rightHandSide).ToString("x8");

            return xor;
        }

        private static string StringToHex(string hex)
        {
            byte[] ba = Encoding.Default.GetBytes(hex);

            var hexString = BitConverter.ToString(ba);

            hexString = hexString.Replace("-", "");

            return hexString;
        }

        private static uint StringToUint(string s)
        {
            return uint.Parse(s, System.Globalization.NumberStyles.HexNumber);
        }


        private static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}