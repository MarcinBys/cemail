﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CEmail
{
    /// <summary>
    /// Logika interakcji dla klasy HomePage.xaml
    /// </summary>
    public partial class HomePage : Page
    {
        public static Frame ContentFrame
        {
            get;
            set;
        }

        public static Frame MailsFrame
        {
            get;
            set;
        }

        public HomePage()
        {
            InitializeComponent();
            ContentFrame = contentFrame;
            MailsFrame = mailsFrame;

            LoadFolder("INBOX");

            Clear();
        }

        /// <summary>
        /// Oczyszcza zawartosc ramki ContentFrame.
        /// </summary>
        public static void Clear()
        {
            StackPanel panel = new StackPanel();

            ContentFrame.Content = panel;
        }

        /// <summary>
        /// Zaladuj podany folder mailowy do ramki na wiadomosci email.
        /// </summary>
        /// <param name="name">nazwa folderu</param>
        private void LoadFolder(string name)
        {
            MailsFrame.Content = new FolderMessagesPage(name);
        }
        
        /// <summary>
        /// Zaladuj ponownie (odswiez) zawartosc ramki z wiadomosciami email.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            LoadFolder("INBOX");
        }

        /// <summary>
        /// Wywolaj ramke do tworzenia i wysylania nowej wiadomosci email.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            ContentFrame.Content = new CreateAndSend();
        }
    }
}
